<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="fr.univlyon1.m1if.m1if03.classes.Billet" %>
<%@ page import="fr.univlyon1.m1if.m1if03.classes.Groupe" %>
<%@ page import="java.util.HashMap" %>
<%@ page import="java.util.Map" %>

<jsp:useBean id="billet" type="fr.univlyon1.m1if.m1if03.classes.Billet" scope="request" />
<jsp:useBean id="billetIndex" type="java.lang.Integer" scope="request" /> 


<!DOCTYPE html>
<html>
<head>
    <title>Billet</title>
</head>
<body>
<h1>Billet : ${billet.titre}</h1>
<h3>Groupe : ${billet.nomGroupe}</h3>
<%-- <p>Auteur :  ${billet.auteur}</p> --%>
<p>Contenu :  ${billet.contenu}</p>


<h3>Commentaires</h3>
<ul>
<c:forEach items="${billet.getCommentaires()}" var="commentaire">
        
            <%-- <li><c:out value="${commentaire.auteur}"/> : --%><li> <c:out value="${commentaire.commentaire}"/></li>   
        

</c:forEach>
</ul>
<%-- <p>Commentaire :  ${billet.commentaires}</p> --%>
<hr>

<form method="post" action="BilletController">
    Ajouter un commentaire :
    <input type="text" name="commentaire" required >
    <input type="hidden" name="selectedGroupe" value="${billet.nomGroupe}">
    <input type="hidden" name="billetIndex" value="${billetIndex}">
    <input type="submit" value="Ajouter">
</form> 

<c:url var="urlD" value="/index.html"></c:url>
    <a href="${urlD}">déconnexion</a>

<input type="button" value="Back" onclick="window.location.href = 'BilletsAff.jsp';">

</body>

</html>
