<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<jsp:useBean id="dto" type="fr.univlyon1.m1if.m1if03.classes.ObjectDTO" scope="application" />

<!DOCTYPE html>
<html>
<head>
    <title>Groupes</title>
</head>
<body>
    <h1>Groupe : ${dto.grName}</h1>
    <%-- <p>Proprietaire :  ${groupes.get(selectedGroupe).getProprietaire()}</p> --%>
    <p>Description :  ${dto.groupe.description}</p>
    <p>
        Users :
        <ul>
        <c:forEach items="${dto.groupe.users}" var="user">
            <li>${user}</li>
        </c:forEach>
        </ul>
    </p>

        <form  method="post" action="User">

        <h3>Ajouter un membre : </h3>
        <input type="text" name="userAdded" required>

        <input type="hidden" name="selectedGroupe" value="${groupes.get(selectedGroupe).getNom()}">
        <input type="submit" value="Ajouter">
        </form>

        <form  method="get" action="BilletController">
        <input type="hidden" name="selectedGroupe" value="${groupes.get(selectedGroupe).getNom()}">
        <input type="submit" value="Billets">
        </form>
    <c:url var="urlD" value="/index.html"></c:url>
    <a href="${urlD}">déconnexion</a>
    <input type="button" value="Back" onclick="<%
        dto.setGrName(null);
        request.getSession().setAttribute("view", "ViewListGroupe");
    %> window.location.reload();">
</body>

</html>
