<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<jsp:useBean id="groupes" type="java.util.Map" scope="application" />
<jsp:useBean id="selectedGroupe" type="java.lang.String" scope="request" />


<html>
<head>
    <title>Billets</title>
</head>
<body>
<div>
    <h3>Billets : </h3>
    <ul>
<c:forEach items="${groupes.get(selectedGroupe).getGBillets().getBillets()}" var="billet" varStatus="loop">
            <li>
                <c:url var="url" value="BilletController">
                    <c:param name="billetIndex" value="${loop.index}"/>
                    <c:param name="selectedGroupe" value="${selectedGroupe}"/>
                </c:url>
                <a href="${url}">${billet.getTitre()}</a>
            </li>
        </c:forEach>
    </ul>
</div>
<hr>
<div>
     <h1>Saisie d'un billet</h1>
    <form method="post" action="BilletController">
        <p>
            Titre :
            <input type="text" name="titre" required>
        </p>
        <input type="hidden" name="selectedGroupe" value=${groupes.get(selectedGroupe).getNom()}>
        <p>
            Contenu :
            <textarea name="contenu"></textarea>
        </p>


        
        
        
            
        <p>
            <input type="submit" value="Envoyer">
        </p>

    </form>
</div>
<c:url var="urlD" value="/index.html"></c:url>
    <a href="${urlD}">déconnexion</a>
</body>
</html>
