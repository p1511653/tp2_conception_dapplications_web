<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--<%@ page contentType="text/html;charset=UTF-8" language="java" %>--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<jsp:useBean id="dto" type="fr.univlyon1.m1if.m1if03.classes.ObjectDTO" scope="application"/>

<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Groupes Form</title>
    <script
            src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js">
    </script>
</head>
<body>
<div>
    <h3>Creation d'un groupe</h3>
    <form id="groupeForm" accept-charset="UTF-8">
        Nom du groupe : <input type="text" name="nomGroupe" required/>
        Description : <input type="text" name="descGroupe"/>
        <input type="submit" value="Ajouter">
    </form>
</div>
<hr>
<div>
    <h3>Selection du groupe</h3>
    <ul>
        <%=dto.getGroupesNames().size()%>
        <c:forEach items="${dto.groupesNames}" var="gr">
            <li>
                <%--<c:url var="url" value="groupes/${dto.grName}">
                    <c:param name="selectedGroupe" value="${gr}"/>
                </c:url>--%>
                <a href="<c:url value="/groupes/${gr}"/>">${gr}</a>
            </li>
        </c:forEach>
    </ul>
    <div id="msg"></div>
</div>
<%--<c:url var="urlD" value="/index.html"></c:url>
<a href="${urlD}">déconnexion</a>--%>
</body>
</html>

<script>
    $(document).ready(function () {
        $("#groupeForm").submit(function (event) {
            event.preventDefault();
            var $form = $(this);
            var nomGroupe = $form.find('input[name="nomGroupe"]').val();
            var url = 'groupes';
            var descGroupe = $form.find('input[name="descGroupe"]').val();
            console.log(nomGroupe);

            $.ajax({
                type: 'POST',
                url: url,
                contentType: 'application/json',
                data: JSON.stringify({
                    nom: nomGroupe,
                    description: descGroupe,
                    proprietaire: "andrew",
                    users: ["andrew", "Simon"]
                }),
                success: function (data, status, xhr) {
                    window.location.reload();
                },
                error: function (xhr, status, error) {
                    $('#msg').html('<span style=\'color:red;\'>' + error + '</span>')
                }
            });
        });
    });

</script>