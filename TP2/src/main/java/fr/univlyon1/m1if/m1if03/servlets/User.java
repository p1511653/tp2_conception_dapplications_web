package fr.univlyon1.m1if.m1if03.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import fr.univlyon1.m1if.m1if03.classes.Groupe;


@WebServlet(name = "user", urlPatterns = {})
public class User extends HttpServlet {

    private ServletContext sc;
    private String[] path;

    public void init(ServletConfig config) throws ServletException {
        sc = config.getServletContext();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        List<User> users = (List<User>) request.getServletContext().getAttribute("users");

        PrintWriter p = response.getWriter();

        this.path = ((String[]) sc.getAttribute("path"));

        p.println("Il y a " + users.size()+ " users");
        if(path.length == 1) {
            for(User u : users) {
                p.println(u);
            }
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }
}
