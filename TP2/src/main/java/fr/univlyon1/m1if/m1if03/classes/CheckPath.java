package fr.univlyon1.m1if.m1if03.classes;

public class CheckPath {
    private String [] path;
    private Groupe groupe;
    private Billet billet;
    private Commentaire commentaire;

    public CheckPath(String fullPath, Groupe groupe,Billet billet,Commentaire commentaire ) {
        this.path = (String[]) fullPath.split("/");
        this.groupe = groupe;
        this.billet = billet;
        this.commentaire = commentaire;
    }

    public CheckPath(String[] fullPath, Groupe groupe, Billet billet,Commentaire commentaire ) {
        this.path = fullPath;
        this.groupe = groupe;
    }

    public boolean validPath(){
        return path.length > 0 && path.length < 8;
    }

    public boolean ifGroupes(){
        return path.length==2 && path[1].equals("groupes");
    }

    public boolean ifIdGroupe(){
        return path.length==3 && path[1].equals("groupes") /*&& (groupe != null)*/ ;
    }

    public String getIdGroupe(){
        return path[3] ;
    }

    public boolean ifBillets(){
        return path.length==4 && path[3].equals("billets") && path[1].equals("groupes") /*&& (groupe != null)*/ ;
    }

    public boolean ifIdBillet(){
        return path.length==5 && path[3].equals("billets") && path[1].equals("groupes")/* && (groupe != null) && (billet != null) */;
    }

    public boolean ifCommentaires(){
        return path.length==6 && path[5].equals("commentaires") && path[3].equals("billets") && path[1].equals("groupes")/* && (groupe != null) && (billet != null) */;
    }

    public boolean ifIdCommentaire(){
        return path.length==7 && path[5].equals("commentaires") && path[3].equals("billets") && path[1].equals("groupes")/* && (groupe != null) && (billet != null) */;
    }

    /*

    0 -> tp2
    1 -> groupes
    2 -> id groupes
    3 -> billets
    4 -> id billets
    5 -> commentaires
    6 -> id commentaires

     */

}
