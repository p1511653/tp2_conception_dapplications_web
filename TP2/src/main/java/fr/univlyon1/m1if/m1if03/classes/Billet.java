package fr.univlyon1.m1if.m1if03.classes;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Billet {
    private String titre, contenu, auteur;
    private String nomGroupe;
    private List<Commentaire> commentaires;
    
    public Billet() {
        this.titre = "Rien";
        this.contenu = "Vide";
        this.auteur = "Personne";
        this.nomGroupe = "Pas de Groupe!";
        commentaires = new ArrayList<Commentaire>() ;
    }

    public Billet(String titre, String contenu, String auteur, String commentaires, String groupe) {
        this.titre = titre;
        this.contenu = contenu;
        this.auteur = auteur;
        this.nomGroupe = groupe;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public String getContenu() {
        return contenu;
    }

    public void setContenu(String contenu) {
        this.contenu = contenu;
    }

    public String getAuteur() {
        return auteur;
    }

    public void setAuteur(String auteur) {
        this.auteur = auteur;
    }

    public String getNomGroupe() {
        return nomGroupe;
    }

    public void setNomGroupe(String groupe) {
        this.nomGroupe = groupe;
    }

    public void addCommentaire(Commentaire commentaire) {
        this.commentaires.add(commentaire);
    }

    public List<Commentaire> getCommentaires() {
        return commentaires;
    }

    public Commentaire getCommentaireById(int id) {
        for (Commentaire comm : this.commentaires) {
            if(comm.getId() == id) {
                return comm;
            }
        }
        return null;
    }

    public void removeCommentaireById(int id) {
        for (Commentaire comm : commentaires) {
            if(comm.getId() == id){
               commentaires.remove(comm);
            }
        }
    }

    public void setCommentaires(List<Commentaire> commentaires) {
        this.commentaires = commentaires;
    }

    @Override
    public String toString() {
        String comList= "\n";
        for(Commentaire commentaire : this.commentaires) {
            comList += commentaire.getAuteur() + " : ";
        }
        String stBillet = "Nom Du Billet : " + this.titre +
                "\nContenu : " + this.contenu +
                "\nAuteur : " + this.auteur +
                "\nCommentaires : " + comList + "\n";
        return stBillet;
    }
}
