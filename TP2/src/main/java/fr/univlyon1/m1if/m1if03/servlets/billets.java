package fr.univlyon1.m1if.m1if03.servlets;

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.univlyon1.m1if.m1if03.classes.*;
import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.univlyon1.m1if.m1if03.classes.Billet;
import fr.univlyon1.m1if.m1if03.classes.Groupe;

@WebServlet(name = "billets", urlPatterns = {})
public class billets extends HttpServlet {

    private ServletContext sc;
    private ObjectDTO dto;

    public void init(ServletConfig config) throws ServletException {
        sc = config.getServletContext();
        dto = (ObjectDTO) sc.getAttribute("dto");
    }

    public void routerGET(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        PrintWriter p = response.getWriter();

        if (dto.getGrName() != null && dto.getGroupe() != null) {

            if (dto.getBiName() != null)
                if (dto.getBillet() != null)
                    p.println(dto.getBillet());
                else p.println("Il n'y a pas un billet avec cet id");
            else if (dto.getBillet() == null) {

                p.println("Affichage Liste Billets du groupe  " + dto.getGrName());
                p.println("Il y a " + dto.getBilletsSize() + " Billets :");

                for (String biName : dto.getBilletsNames()) {
                    p.println(biName);
                }

                request.setAttribute("directory", "ViewList");

            } else {

                p.println("Affichage d'un Billet : ");
                p.println(dto.getBillet());
                request.setAttribute("directory", "ViewSingle");

            }
        } else {
            p.println("ERREUR avec le groupe");
        }
    }


    public void routerPOST(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        PrintWriter p = response.getWriter();

        if (dto.getGrName() != null && dto.getGroupe() != null) {
            if (dto.getBiName() != null)
                p.println("Veuillez ajouter un billet valide");
            else if (dto.getBillet() == null) {

                p.println("Ajout d'un  Billet");

                String res = IOUtils.toString(request.getReader());

                Billet billet = new ObjectMapper().readValue(res, Billet.class);

                dto.setBiName(billet.getTitre());

                dto.addBillet(billet);

                p.println(billet);

            } else {
                p.println("Il y a deja un billet avec ce nom!");
            }
        } else {
            p.println("ERREUR avec le groupe");
        }
    }

    public void routerPUT(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        PrintWriter p = response.getWriter();

        if (dto.getGrName() != null && dto.getGroupe() != null) {
            if (dto.getBiName() != null) {

                p.println("Modification d'un  Billet");

                String res = IOUtils.toString(request.getReader());
                Billet billet = new ObjectMapper().readValue(res, Billet.class);

                if (dto.getBillet() != null) {
                    dto.setBiName(billet.getTitre());

                    dto.modifyBillet(billet);

                    p.println(billet);
                } else {
                    dto.setBiName(billet.getTitre());

                    dto.addBillet(billet);

                    p.println(billet);
                }

            } else {
                p.println("Veuillez presiser le nom du billet");
            }
        } else {
            p.println("ERREUR avec le groupe");
        }
    }

    public void routerDELETE(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        PrintWriter p = response.getWriter();

        if (dto.getGrName() != null && dto.getGroupe() != null) {
            if (dto.getBiName() != null) {
                if (dto.getBillet() != null) {

                    p.println("Suppression d'un billet");

                    dto.removeCurrentBillet();

                    request.setAttribute("directory", "ViewList");

                } else {

                    p.println("Il n'y a pas un billet avec cet id!");
                    request.setAttribute("directory", "ViewSingle");

                }
            } else {
                p.println("Veuillez presiser le nom du billet");
            }
        } else {
            p.println("ERREUR avec le groupe");
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        routerPOST(request, response);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        routerGET(request, response);
    }

    @Override
    protected void doPut(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        routerPUT(request, response);
    }

    @Override
    protected void doDelete(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        routerDELETE(request, response);
    }
}