package fr.univlyon1.m1if.m1if03.classes;

import java.util.ArrayList;
import java.util.List;

public class GestionBillets {
    private List<Billet> billets;

    public GestionBillets() {
        this.billets = new ArrayList<>();
    }

    public void add(Billet billet) {
        for (Billet b : billets) {
            if (b.getTitre() == null ? billet.getTitre() == null : b.getTitre().equals(billet.getTitre()))
                return;
        }
        this.billets.add(billet);
    }

    public int size() {
        return this.billets.size();
    }

    public Billet getBillet(int i) {
        return billets.get(i);
    }

    public Billet getBilletByName(String name) {
        for (Billet billet : billets) {
            if(billet.getTitre().equals(name)){
                return billet;
            } 
        }
        return null;
    }

    public void removeBilletByName(String name) {
        for (Billet billet : billets) {
            if(billet.getTitre().equals(name)){
                billets.remove(billet);
            }
        }
    }

    public List<Billet> getBilletsParGroupe(String groupe) {
        List<Billet> list = new ArrayList<>();
        for (Billet b : billets) {
            if (groupe.equals(b.getNomGroupe())) list.add(b);
        }
        return list;
    }

    public List<Billet> getBillets() {
        return this.billets;
    }

    public Billet getLastBillet() {
        if (billets.size() > 0)
            return this.getBillet(billets.size() - 1);
        throw new IndexOutOfBoundsException("Erreur dans l'appel à la fonction getLastBillet");
    }
}
