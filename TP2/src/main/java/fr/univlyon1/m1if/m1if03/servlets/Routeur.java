package fr.univlyon1.m1if.m1if03.servlets;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.net.URI;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.univlyon1.m1if.m1if03.classes.ObjectDTO;

@WebServlet(name = "Routeur", urlPatterns = "/*")
public class Routeur extends HttpServlet {

    private ServletContext sc;
    ObjectDTO dto = new ObjectDTO();


    public void init(ServletConfig config) throws ServletException {
        sc = config.getServletContext();
        sc.setAttribute("dto", dto);
    }

    public void router(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, InterruptedException {
        String url = (String) request.getPathInfo();
        String[] path = (String[]) url.split("/");

        request.setAttribute("path", path);
        sc.setAttribute("path", path);

        System.out.println(path.length);

        sc.setAttribute("dto", dto);

        if ((path.length == 2 || path.length == 3) && path[1].equals("user")) {
            request.getServletContext().getNamedDispatcher("user").forward(request, response);
        }

        if ((path.length == 2) || (path.length == 3)) {
            if (path.length == 3)
                dto.setGrName(path[2]);
            else dto.setGrName(null);
            request.getServletContext().getNamedDispatcher("groupes").forward(request, response);
        } else if ((path.length == 4) || (path.length == 5)) {
            dto.setGrName(path[2]);
            if (path.length == 5)
                dto.setBiName(path[4]);
            else dto.setBiName(null);
            request.getServletContext().getNamedDispatcher("billets").forward(request, response);
        } else if ((path.length == 6) || (path.length == 7)) {
            dto.setGrName(path[2]);
            dto.setBiName(path[4]);
            if (path.length == 7)
                dto.setComId(Integer.parseInt(path[6]));
            else dto.setComId(-1);
            request.getServletContext().getNamedDispatcher("commentaires").forward(request, response);
        } else {
            response.getWriter().println("mauvaise route routeur");
        }

//        response.setHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
        /*response.setHeader("Authorization", null);
        response.setHeader("Cookie", "JSESSIONID=6972511993C2FCA8586062F2593FDE93; token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJvbyIsImF1ZCI6Imh0dHA6Ly8xOTIuMTY4Ljc1LjEzOjgwODAvdjIiLCJjb3IiOiJDb3JyZWN0aW9uIiwiaXNzIjoiTWVzIENvcGFpbnMiLCJleHAiOjE1NzQ3ODQ2Njl9.E9UxCnUhw0AG3V7G3Mt1rj2DQ2EgPt9TVhMPgHKMYlA; JSESSIONID=FD03F3BAF4C0CD126A57E63601788DE1;");
        response.sendRedirect("http://192.168.75.13:8080/v2/groupes");
        response.setStatus(response.SC_MOVED_TEMPORARILY);
  response.setHeader("Location", site);*/

//        String charset = "UTF-8";
//
//        String query = String.format("pseudo=%s",
//                URLEncoder.encode("oo", charset));
//
//        URLConnection connection = new URL("http://192.168.75.13:8080/v2/groupes").openConnection();
//        connection.setRequestProperty("Request-Method","GET");
//        connection.setDoOutput(true);
//        connection.setRequestProperty("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
//        connection.setRequestProperty("Cookie", "JSESSIONID=6972511993C2FCA8586062F2593FDE93; token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJvbyIsImF1ZCI6Imh0dHA6Ly8xOTIuMTY4Ljc1LjEzOjgwODAvdjIiLCJjb3IiOiJDb3JyZWN0aW9uIiwiaXNzIjoiTWVzIENvcGFpbnMiLCJleHAiOjE1NzQ3ODQ2Njl9.E9UxCnUhw0AG3V7G3Mt1rj2DQ2EgPt9TVhMPgHKMYlA; JSESSIONID=FD03F3BAF4C0CD126A57E63601788DE1;");
//        connection.setRequestProperty("Authorization", null);
//        try {
//            connection.getOutputStream().write(query.getBytes(charset));
//        }
//        finally {
//            connection.getOutputStream().close();
//        }
//        InputStream resp = connection.getInputStream();
//        System.out.println();

        /*HttpClient client = HttpClient.newHttpClient();
        HttpRequest req = HttpRequest.newBuilder()
                .uri(URI.create("http://192.168.75.13:8080/v2"))
                .build();

        HttpResponse<String> resp = client.send((HttpRequest) request, HttpResponse.BodyHandlers.ofString());

        System.out.println(resp.body());*/
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            router(request, response);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        PrintWriter p = response.getWriter();
        try {
            router(request, response);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    @Override
    protected void doPut(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        PrintWriter p = response.getWriter();
        try {
            router(request, response);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    @Override
    protected void doDelete(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        PrintWriter p = response.getWriter();
        try {
            router(request,response);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}