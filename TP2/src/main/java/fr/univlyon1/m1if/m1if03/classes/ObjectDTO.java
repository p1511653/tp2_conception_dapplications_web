package fr.univlyon1.m1if.m1if03.classes;

import fr.univlyon1.m1if.m1if03.servlets.User;

import java.util.*;

public class ObjectDTO {
    private Map<String, Groupe> groupes;
    private List<User> users;
    private String grName;
    private String biName;
    private int comId;

    public ObjectDTO() {
        this.groupes = new HashMap<>();
        this.users = new ArrayList<>();
        this.grName = null;
        this.biName = null;
        this.comId = -1;
    }

    // Groupe Functions

    public String getGrName() {
        return grName;
    }

    public void setGrName(String grName) {
        this.grName = grName;
    }

    public Groupe getGroupe(String gName) {
        return groupes.get(gName);
    }

    public Groupe getGroupe() {
        return groupes.get(grName);
    }

    public int getGroupesSize() {
        return groupes.size();
    }

    public Set<String> getGroupesNames() {
        return groupes.keySet();
    }

    public void addGroupe(Groupe gr) {
        this.groupes.put(grName, gr);
    }

    public void removeCurrentGroupe() {
        if (grName != null && groupes.get(grName) != null) {
            groupes.remove(grName);
            grName = null;
        }
    }

    public boolean containsGroupe(Groupe gr) {
        return groupes.get(gr) != null;
    }

    public void modifyGroupe(Groupe gr) {
        getGroupe().setProprietaire(gr.getProprietaire());
        getGroupe().setDescription(gr.getDescription());
        getGroupe().setgBillets(gr.getGBillets());
        getGroupe().setUsers(gr.getUsers());
    }

    // Billet Functions

    public String getBiName() {
        return biName;
    }

    public void setBiName(String biName) {
        this.biName = biName;
    }

    public int getBilletsSize() {
        return getGroupe().getGBillets().size();
    }

    public GestionBillets getAllBillets(String bName) {
        return getGroupe().getGBillets();
    }

    public Billet getBillet() {
        return getGroupe().getGBillets().getBilletByName(biName);
    }

    public Billet getBillet(String bName) {
        return getGroupe().getGBillets().getBilletByName(bName);
    }

    public Set<String> getBilletsNames() {
        Set<String> biNames = new HashSet<>();
         for(Billet b : getGroupe().getGBillets().getBillets()) {
             biNames.add(b.getTitre());
         }
         return biNames;
    }

    public boolean containsBillet(Billet bi) {
        return getGroupe().getGBillets().getBillets().contains(bi);
    }

    public void removeCurrentBillet() {
        if (grName != null && biName != null && getGroupe(grName) != null && getBillet(biName) != null) {
            getGroupe().getGBillets().removeBilletByName(biName);
            biName = null;
        }
    }

    public void addBillet(Billet b) {
        getGroupe().getGBillets().add(b);
    }

    public void modifyBillet(Billet b) {
        getBillet().setNomGroupe(b.getNomGroupe());
        getBillet().setAuteur(b.getAuteur());
        getBillet().setContenu(b.getContenu());
        getBillet().setCommentaires(b.getCommentaires());
    }

    // Commentaire Functions

    public int getComId() {
        return comId;
    }

    public void setComId(int commentaire) {
        this.comId = commentaire;
    }

    public int getCommsSize() {
        return getBillet().getCommentaires().size();
    }

    public Commentaire getCommentaire() {
        return getBillet().getCommentaireById(comId);
    }

    public List<Commentaire> getCommentaires() {
        return getBillet().getCommentaires();
    }

    public Commentaire getCommentaire(int cId) {
        return getBillet().getCommentaireById(cId);
    }

    public void addCommentaire(Commentaire comm) {
        getBillet().addCommentaire(comm);
    }

    public void removeCommentaire() {
        getBillet().removeCommentaireById(comId);
    }

    public void modifyCommentaire(Commentaire c) {
        getCommentaire().setAuteur(c.getAuteur());
        getCommentaire().setCommentaire(c.getCommentaire());
        getCommentaire().setId(c.getId());
    }
}
