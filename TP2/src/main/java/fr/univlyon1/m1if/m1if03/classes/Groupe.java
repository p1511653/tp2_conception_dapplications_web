package fr.univlyon1.m1if.m1if03.classes;

import java.util.ArrayList;
import java.util.List;

public class Groupe {
    private String nom;
    private String description;
    private String proprietaire;
    private List<String> users;
    private GestionBillets gBillets;

    public Groupe() {
        nom = "";
        description = "";
        proprietaire = "personne";
        users = new ArrayList<>();
        gBillets = new GestionBillets();
    }

    public Groupe(String nom, String description, String proprietaire, List<String> users, GestionBillets gBillets) {
        this.nom = nom;
        this.description = description;
        this.proprietaire = proprietaire;
        this.users = users;
        this.gBillets = gBillets;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getProprietaire() {
        return proprietaire;
    }

    public void setProprietaire(String proprietaire) {
        this.proprietaire = proprietaire;
    }

    public List<String> getUsers() {
        return users;
    }

    public void setUsers(List<String> users) {
        this.users = users;
    }

    public void addUser(String user) {
        this.users.add(user);
    }

    public GestionBillets getGBillets() {
        return gBillets;
    }

    public void setgBillets(GestionBillets gBillets) {
        this.gBillets = gBillets;
    }

    @Override
    public String toString() {
        String userList= "";
        for(String user : this.users) {
            userList += user + ", ";
        }
        String stGroupe = "Nom Du Groupe : " + this.nom +
                "\nDescription : " + this.description +
                "\nProprietaire : " + this.proprietaire +
                "\nUsers : " + userList;
        return stGroupe;
    }
}
