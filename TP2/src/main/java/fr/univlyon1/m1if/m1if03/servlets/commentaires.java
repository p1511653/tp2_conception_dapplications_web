package fr.univlyon1.m1if.m1if03.servlets;

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.univlyon1.m1if.m1if03.classes.*;
import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.univlyon1.m1if.m1if03.classes.Billet;


@WebServlet(name = "commentaires", urlPatterns = {})
public class commentaires extends HttpServlet {

    private ServletContext sc;
    private ObjectDTO dto;

    public void init(ServletConfig config) throws ServletException {
        sc = config.getServletContext();
        dto = (ObjectDTO) sc.getAttribute("dto");
    }

    public void routerGET(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        PrintWriter p = response.getWriter();

        if (dto.getGrName() != null && dto.getGroupe() != null) {
            if (dto.getBiName() != null && dto.getBillet() != null) {

                if (dto.getComId() != -1)
                    if (dto.getCommentaire() != null)
                        p.println(dto.getCommentaire());
                    else p.println("Il n'y a pas un commentaire avec cet id");
                else if (dto.getCommentaire() == null) {

                    p.println("Affichage Liste Commentaire du groupe  " + dto.getGrName() + " Dans le billet " + dto.getBiName());
                    p.println("Il y a " + dto.getCommsSize() + " Commentaires :");

                    for (Commentaire com : dto.getCommentaires()) {
                        p.println(com);
                    }

                    request.setAttribute("directory", "ViewList");

                } else {

                    p.println("Affichage d'un Commentaire : ");
                    p.println(dto.getCommentaire());
                    request.setAttribute("directory", "ViewSingle");

                }
            } else {
                p.println("ERREUR avec le billet");
            }
        } else {
            p.println("ERREUR avec le groupe");
        }
    }

    public void routerPOST(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter p = response.getWriter();

        if (dto.getGrName() != null && dto.getGroupe() != null) {
            if (dto.getBiName() != null && dto.getBillet() != null) {

                if (dto.getComId() != -1)
                    p.println("Veuillez ajouter un Commentaire valide");
                else if (dto.getCommentaire() == null) {

                    p.println("Ajout d'un  Commentaire");

                    String res = IOUtils.toString(request.getReader());

                    Commentaire com = new ObjectMapper().readValue(res, Commentaire.class);

                    dto.setComId(com.getId());

                    dto.addCommentaire(com);

                    p.println(com);

                } else {
                    p.println("Il y a deja un Commentaire avec cet id!");
                }
            } else {
                p.println("ERREUR avec le billet");
            }
        } else {
            p.println("ERREUR avec le groupe");
        }
    }

    public void routerPUT(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter p = response.getWriter();

        if (dto.getGrName() != null && dto.getGroupe() != null) {
            if (dto.getBiName() != null && dto.getBillet() != null) {

                if (dto.getComId() != -1) {

                    p.println("Modification d'un Commentaire");

                    String res = IOUtils.toString(request.getReader());
                    Commentaire com = new ObjectMapper().readValue(res, Commentaire.class);

                    if (dto.getCommentaire() != null) {
                        dto.setComId(com.getId());

                        dto.modifyCommentaire(com);

                        p.println(com);
                    } else {
                        dto.setComId(com.getId());

                        dto.addCommentaire(com);

                        p.println(com);
                    }

                } else {
                    p.println("Veuillez presiser l'id du Commentaire");
                }
            } else {
                p.println("ERREUR avec le billet");
            }
        } else {
            p.println("ERREUR avec le groupe");
        }
    }

    public void routerDELETE(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter p = response.getWriter();

        if (dto.getGrName() != null && dto.getGroupe() != null) {
            if (dto.getBiName() != null && dto.getBillet() != null) {

                if (dto.getComId() != -1) {
                    if (dto.getCommentaire() != null) {

                        p.println("Suppression d'un commentaire");

                        dto.removeCommentaire();

                        request.setAttribute("directory", "ViewList");

                    } else {

                        p.println("Il n'y a pas un commentaire avec cet id!");
                        request.setAttribute("directory", "ViewSingle");

                    }
                } else {
                    p.println("Veuillez presiser l'id du Commentaire");
                }
            } else {
                p.println("ERREUR avec le billet");
            }
        } else {
            p.println("ERREUR avec le groupe");
        }
    }


    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        routerPOST(request, response);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        routerGET(request, response);
    }

    @Override
    protected void doPut(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        routerPUT(request, response);
    }

    @Override
    protected void doDelete(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        routerDELETE(request, response);
    }

}
