package fr.univlyon1.m1if.m1if03.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.univlyon1.m1if.m1if03.classes.Groupe;

@WebServlet(name = "GroupeController", urlPatterns = "/GroupesController")
public class GroupeController extends HttpServlet {

    private Map<String, Groupe> groupes;
    private ServletContext sc;

    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        groupes = new HashMap<>();
        sc = getServletContext();
        sc.setAttribute("groupes", groupes);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String pseudo = (String) request.getSession().getAttribute("pseudo");
        String nomGroupe = (String) request.getParameter("nomGroupe");
        String description = (String) request.getParameter("descGroupe");
        //String userAdded = (String) request.getParameter("userAdded");
        //String selectedGroupe = (String) request.getParameter("selectedGroupe");

        // if (userAdded != null) {
        //     if (!groupes.get(selectedGroupe).getUsers().contains(userAdded)) {
        //         groupes.get(selectedGroupe).addUser(userAdded);
        //     }
        //     request.setAttribute("selectedGroupe", selectedGroupe);
        //     request.getRequestDispatcher("WEB-INF/jsp/GroupeAff.jsp").forward(request, response);
            
        // } else 
        /*if (groupes.get(nomGroupe) == null) {
            Groupe gr = new Groupe();
            gr.setNom(nomGroupe);
            gr.setDescription(description);
            // gr.setProprietaire(pseudo);
            // gr.addUser(pseudo);
            groupes.put(nomGroupe, gr);
            request.getRequestDispatcher("WEB-INF/jsp/GroupeForm.jsp").forward(request, response);

        } else {
            groupes.get(pseudo);
            request.getRequestDispatcher("WEB-INF/jsp/GroupeForm.jsp").forward(request, response);

        }*/
        request.getSession().setAttribute("pseudo", request.getParameter("pseudo"));


        PrintWriter p = response.getWriter();
        String groupeForm = "<html lang=\"en\">\n" +
                "<head>\n" +
                "    <meta charset=\"UTF-8\">\n" +
                "    <title>Groupes Form</title>\n" +
                "</head>\n" +
                "<body>\n" +
                "    <div>\n" +
                "        <h3>Creation d'un groupe</h3>\n" +
                "        <form method=\"post\" action=\"GroupeController\">\n" +
                "            Nom du groupe : <input type=\"text\" name=\"nomGroupe\" required>\n" +
                "            Description : <textarea name=\"descGroupe\" ></textarea>\n" +
                "            <input type=\"submit\" value=\"Ajouter\">\n" +
                "        </form>\n" +
                "    </div>\n" +
                "    <hr>\n" +
                "    <div>\n" +
                "        <h3>Selection du groupe</h3>\n" +
                "        <ul>\n" +
                "            <c:forEach items=\"${groupes}\" var=\"gr\">\n" +
                "                <li>\n" +
                "                    <c:url var=\"url\" value=\"GroupeController\">\n" +
                "                        <c:param name=\"selectedGroupe\" value=\"${gr.key}\"/>\n" +
                "                    </c:url>\n" +
                "                    <a href=\"${url}\">${gr.key}</a>\n" +
                "                </li>\n" +
                "            </c:forEach>\n" +
                "        </ul>\n" +
                "    </div>\n" +
                "    <c:url var=\"urlD\" value=\"/index.html\"></c:url>\n" +
                "    <a href=\"${urlD}\">déconnexion</a>\n" +
                "</body>\n" +
                "</html>";
//        p.print(groupeForm);

        response.getWriter().println("Groupes POST");
        return;
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        String pseudo = (String) request.getSession().getAttribute("pseudo");
        String selectedGroupe = (String) request.getParameter("selectedGroupe");
        request.setAttribute("selectedGroupe", selectedGroupe);
        /*if (selectedGroupe != null) {

            // request.getSession(true).setAttribute("selectedGroupe",
            // request.getParameter("selectedGroupe"));
            request.getRequestDispatcher("WEB-INF/jsp/GroupeAff.jsp").forward(request, response);
        } else
            request.getRequestDispatcher("WEB-INF/jsp/GroupeForm.jsp").forward(request, response);*/
        response.getWriter().println("Groupes Get");
        return;

    }

}
