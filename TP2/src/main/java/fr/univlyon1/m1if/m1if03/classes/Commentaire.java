package fr.univlyon1.m1if.m1if03.classes;

public class Commentaire{

    private String auteur, commentaire;
    private int id;

    public Commentaire(){
        this.id = 0;
        this.auteur = "vide";
        this.commentaire = "vide";

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAuteur() {
        return auteur;
    }

    public void setAuteur(String auteur) {
        this.auteur = auteur;
    }

    public String getCommentaire() {
        return commentaire;
    }

    public void setCommentaire(String commentaire) {
        this.commentaire = commentaire;
    }

    @Override
    public String toString() {
        String comList= "Commentaire " + this.id + " de l'auteur " + this.auteur + " : " + this.commentaire;
        return comList;
    }

}