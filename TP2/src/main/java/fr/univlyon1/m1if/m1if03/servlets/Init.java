package fr.univlyon1.m1if.m1if03.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Collections;
import java.util.Date;

import javax.persistence.OneToMany;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "Init", urlPatterns = "/Init")
public class Init extends HttpServlet {

        @Override
        protected void doPost (HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
            response.sendRedirect("GroupeController");
            /*PrintWriter p = response.getWriter();
            p.println("from router " + request.getRequestURI());*/
            //request.getServletContext().getNamedDispatcher("/Groupes/111").forward(request, response);
        }
}
