package fr.univlyon1.m1if.m1if03.servlets;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.univlyon1.m1if.m1if03.classes.Billet;
import fr.univlyon1.m1if.m1if03.classes.Commentaire;
import fr.univlyon1.m1if.m1if03.classes.GestionBillets;
import fr.univlyon1.m1if.m1if03.classes.Groupe;
import jdk.jfr.Frequency;

@WebServlet(name = "BilletController", urlPatterns = "/BilletController", loadOnStartup = 1)
public class BilletController extends HttpServlet {

    private ServletContext sc;
    private long lastModified;

    public void init(ServletConfig config) throws ServletException {
        sc = config.getServletContext();
        lastModified = 0;
    }

    @Override
    protected long getLastModified (HttpServletRequest req) {
        return this.lastModified;
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String pseudo = (String) request.getSession().getAttribute("pseudo");
        String titre = request.getParameter("titre");
        String contenu = request.getParameter("contenu");
        String commentaire = request.getParameter("commentaire");
        String selectedGroupe = request.getParameter("selectedGroupe");
        Map<String, Groupe> groupes = (Map<String, Groupe>) sc.getAttribute("groupes");

        this.lastModified = new Date().getTime();

        if (titre != null && contenu != null) {
            Billet billet = new Billet();
            billet.setTitre(titre);
            billet.setContenu(contenu);
            // billet.setAuteur(pseudo);
            billet.setNomGroupe(selectedGroupe);
            groupes.get(selectedGroupe).getGBillets().add(billet);
            request.setAttribute("selectedGroupe", selectedGroupe);
            request.setAttribute("groupes", groupes);
            request.getRequestDispatcher("WEB-INF/jsp/BilletsAff.jsp").forward(request, response);
        } else if (commentaire != null && !commentaire.equals("")) {
            String billetInd = request.getParameter("billetIndex");
            Integer billetIndex = Integer.parseInt(billetInd);
            Groupe groupe = groupes.get(selectedGroupe);
            GestionBillets gestionBillets = groupe.getGBillets();
            Billet billet = gestionBillets.getBillet(billetIndex);
            Commentaire c = new Commentaire();
            // c.setAuteur(pseudo);
            c.setCommentaire(commentaire);
            billet.getCommentaires().add(c);
            request.setAttribute("billetIndex", billetIndex);
            request.setAttribute("billet", billet);
            request.getRequestDispatcher("WEB-INF/jsp/billet.jsp").forward(request, response);
        } else {
            request.setAttribute("selectedGroupe", selectedGroupe);
            request.getRequestDispatcher("WEB-INF/jsp/BilletsAff.jsp").forward(request, response);
        }

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        String selectedGroupe = request.getParameter("selectedGroupe");
        String billetId = request.getParameter("billetIndex");

        if (billetId != null) {
            Integer billetIndex = Integer.parseInt(billetId);
            HashMap<String, Groupe> groupes = (HashMap<String, Groupe>) sc.getAttribute("groupes");
            Groupe groupe = groupes.get(selectedGroupe);
            GestionBillets gestionBillets = groupe.getGBillets();
            Billet billet = gestionBillets.getBillet(billetIndex);
            request.setAttribute("billetIndex", billetIndex);
            request.setAttribute("billet", billet);
            request.getRequestDispatcher("WEB-INF/jsp/billet.jsp").forward(request, response);
        } else {
            request.setAttribute("selectedGroupe", selectedGroupe);
            request.getRequestDispatcher("WEB-INF/jsp/BilletsAff.jsp").forward(request, response);

        }

    }

}
