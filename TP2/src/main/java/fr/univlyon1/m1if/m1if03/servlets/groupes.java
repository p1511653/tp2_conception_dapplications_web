package fr.univlyon1.m1if.m1if03.servlets;

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.univlyon1.m1if.m1if03.classes.CheckPath;
import fr.univlyon1.m1if.m1if03.classes.Groupe;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;
import java.util.Scanner;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.univlyon1.m1if.m1if03.classes.ObjectDTO;
import org.apache.commons.io.IOUtils;;

@WebServlet(name = "groupes", urlPatterns = {"/groupes/*"})
public class groupes extends HttpServlet {

    private ServletContext sc;
    private ObjectDTO dto;

    public void init(ServletConfig config) throws ServletException {
        sc = config.getServletContext();
        dto = (ObjectDTO) sc.getAttribute("dto");
    }

    private void analysePath(HttpServletRequest request) {
        String[] path = request.getRequestURI().split("/");
        if (path.length == 4) {
            dto.setGrName(path[3]);
        }
        else dto.setGrName(null);
    }

    private void routerGET(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {

        PrintWriter p = response.getWriter();

        analysePath(request);

        System.out.println("GET Groupe");

        if (dto.getGrName() != null)
            if (dto.getGroupe() != null) {
                System.out.println("Affichage un Groupe");
                request.getSession().setAttribute("view", "ViewGroupe");
            }
            else System.out.println("Il n'y a pas un groupe avec cet id");
        else if (dto.getGroupe() == null) {

            System.out.println("Affichage Liste Groupes");

            request.getSession().setAttribute("view", "ViewListGroupe");
        } else {

            System.out.println("Affichage d'un Groupe : ");
            System.out.println(dto.getGroupe());

        }
    }


    private void routerPOST(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        PrintWriter p = response.getWriter();
        System.out.println("POST Groupe");

        if (dto.getGrName() != null)
            System.out.println("Veuillez ajouter un groupe valide");
        else if (dto.getGroupe() == null) {

            System.out.println("Ajout d'un  Groupe");

            String res = IOUtils.toString(request.getReader());

            Groupe groupe = new ObjectMapper().readValue(res, Groupe.class);

            dto.setGrName(groupe.getNom());

            dto.addGroupe(groupe);

            request.getSession().setAttribute("view", "ViewGroupe");

        } else {
            System.out.println("Il y a deja un groupe avec ce nom!");
        }
    }

    private void routerPUT(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        System.out.println("PUT");

        PrintWriter p = response.getWriter();

        if (dto.getGrName() != null) {

            System.out.println("Modification d'un  Groupe");

            String res = IOUtils.toString(request.getReader());
            Groupe groupe = new ObjectMapper().readValue(res, Groupe.class);

            if (dto.getGroupe() != null) {
                dto.setGrName(groupe.getNom());

                dto.modifyGroupe(groupe);

                System.out.println(groupe);
            } else {
                dto.setGrName(groupe.getNom());

                dto.addGroupe(groupe);

                System.out.println(groupe);
            }

        } else {
            System.out.println("Veuillez presiser le nom du groupe");
        }
    }

    private void routerDELETE(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        PrintWriter p = response.getWriter();

        if (dto.getGrName() != null) {
            if (dto.getGroupe() != null) {

                System.out.println("Suppression d'un Groupes");

                dto.removeCurrentGroupe();

                request.getSession().setAttribute("view", "ViewList");

            } else {

                System.out.println("Il n'y a pas un groupe avec cet id!");
                request.getSession().setAttribute("view", "ViewSingle");

            }
        } else {
            System.out.println("Veuillez presiser le nom du groupe");
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        routerPOST(request, response);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        routerGET(request, response);
    }

    @Override
    protected void doPut(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        routerPUT(request, response);
    }

    @Override
    protected void doDelete(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        routerDELETE(request, response);
    }

}
