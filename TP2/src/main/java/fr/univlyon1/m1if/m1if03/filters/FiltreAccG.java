 package fr.univlyon1.m1if.m1if03.filters;

 import java.io.IOException;
 import java.io.PrintWriter;
 import java.util.Map;

 import javax.servlet.*;
 import javax.servlet.annotation.WebFilter;
 import javax.servlet.http.HttpServletRequest;
 import javax.servlet.http.HttpServletRequestWrapper;

 import fr.univlyon1.m1if.m1if03.classes.Groupe;

 public class FiltreAccG implements javax.servlet.Filter {

     FilterConfig filterConfig;

     @Override
     public void init(FilterConfig filterConfig) throws ServletException {
         this.filterConfig = filterConfig;
     }

     public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws ServletException, IOException {
         System.out.println("before doFilter");
         chain.doFilter(req, resp);
         System.out.println("after doFilter");
         String view = (String) ((HttpServletRequest) req).getSession().getAttribute("view");
         // S'il y a un contenu à renvoyer
         if (view != null) {
             // TODO implémenter ici la négociation de contenus
             if(((HttpServletRequest) req).getHeader("Accept").startsWith("text/html")) {
                 // Cas des JSP (nommées dans le web.xml)
                 System.out.println("View : " + view);
                 RequestDispatcher dispatcher = filterConfig.getServletContext().getNamedDispatcher(view);
                 HttpServletRequest wrapped = new HttpServletRequestWrapper((HttpServletRequest) req) {
                     public String getServletPath() {
                         return "";
                     }
                 };
                 dispatcher.forward(wrapped, resp);
             }
         }
         //chain.doFilter(req,resp);
     }

     @Override
     public void destroy() {

     }
 }
