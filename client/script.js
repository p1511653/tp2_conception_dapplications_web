var dataGroupe = {
    nom: "gr2",
    description: "desc",
    proprietaire: "andrew",
    users: [
        "andrew",
        "simon"
    ]
};

var dataBillet = {
    titre: "b1",
    contenu: "desc",
    auteur: "andrew",
    nomGroupe: "gr1",
    commentaires: [{
        id: 0,
        auteur: "andrew",
        commentaire: "comm de andrew"
    }, {
        id: 1,
        auteur: "simon",
        commentaire: "comm de simon"
    }]
};

function mustacheTemp(idScript, data, idHTML) {
    var template = document.getElementById(idScript).innerHTML;
    Mustache.parse(template); // optional, speeds up future uses
    var rendered = Mustache.render(template, data);
    //console.log(rendered);
    document.getElementById(idHTML).innerHTML = rendered;
}

//Cookies.set('token', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJzYSIsImF1ZCI6Imh0dHA6Ly8xOTIuMTY4Ljc1LjEzOjgwODAvdjIiLCJjb3IiOiJDb3JyZWN0aW9uIiwiaXNzIjoiTWVzIENvcGFpbnMiLCJleHAiOjE1NzUxMzgxNTR9.IjVVGtpXvIoWE5_eaGSOn01lXeknXGTOLpyj_1vf6jQ');
//Cookies.set('Accept', "text/html,application/xhtml+xml,application/xml;q=0.9,*!/!*;q=0.8");


// fetch('https://192.168.75.13/api/v2/users/login', {
// method: 'POST',
// //mode: "cors",
// body: JSON.stringify({'pseudo': 'andrew'}),
// headers: {
// 'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*!/!*;q=0.8',
// 'Cookie': 'JSESSIONID=7B2AA870117B4EAB67FE0910772C67E3; token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJzYSIsImF1ZCI6Imh0dHA6Ly8xOTIuMTY4Ljc1LjEzOjgwODAvdjIiLCJjb3IiOiJDb3JyZWN0aW9uIiwiaXNzIjoiTWVzIENvcGFpbnMiLCJleHAiOjE1NzUxMzgxNTR9.IjVVGtpXvIoWE5_eaGSOn01lXeknXGTOLpyj_1vf6jQ',
// }
// }).then(function (response) {
// return response.json();
// }).then(function (json) {
// console.log(json);
// });

/*$.ajax({
url: 'https://192.168.75.13/api/v2/users/login',
type: 'POST',
data: JSON.stringify({pseudo: 'andrew'}),
headers: {
'Accept': 'application/json',
//'Authorization': 'token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJzYSIsImF1ZCI6Imh0dHA6Ly8xOTIuMTY4Ljc1LjEzOjgwODAvdjIiLCJjb3IiOiJDb3JyZWN0aW9uIiwiaXNzIjoiTWVzIENvcGFpbnMiLCJleHAiOjE1NzUxMzgxNTR9.IjVVGtpXvIoWE5_eaGSOn01lXeknXGTOLpyj_1vf6jQ',
'Content-Type': 'application/json',
},
success: function (data, textStatus, request) {
Cookies.set('Authorization', request.getResponseHeader('Authorization'));
}
});*/

// $.ajax({
// url: 'https://192.168.75.13/api/v2/groupes',
// type: 'POST',
// data: JSON.stringify({
// nom: 'GrAndrew',
// description: "test"
// }),
// headers: {
// 'Accept': 'application/json',
// //'Authorization': 'token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJzYSIsImF1ZCI6Imh0dHA6Ly8xOTIuMTY4Ljc1LjEzOjgwODAvdjIiLCJjb3IiOiJDb3JyZWN0aW9uIiwiaXNzIjoiTWVzIENvcGFpbnMiLCJleHAiOjE1NzUxMzgxNTR9.IjVVGtpXvIoWE5_eaGSOn01lXeknXGTOLpyj_1vf6jQ',
// 'Content-Type': 'application/json',
// 'Authorization': Cookies.get('Authorization'),
// },
// success: function (data, textStatus, request) {
// Cookies.set('Authorization', request.getResponseHeader('Authorization'));
// }
// });

function myAjax(url, method, data = null, func) {
    return $.ajax({
        url: url,
        type: method,
        data: data,
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': Cookies.get('Authorization'),
        }
    });
}

$(document).on('submit', 'form', function(e) {
    e.preventDefault();
});

function logIn() {
    myAjax('https://192.168.75.13/api/v2/users/login', 'POST', JSON.stringify({ pseudo: $("#pseudo").val() }))
        .then((d, t, request) => {
            Cookies.set('Authorization', request.getResponseHeader('Authorization'));
            window.location.href = '#groupes';
            fillGroupes();
        });
}

function deconnection() {
    Cookies.set('Authorization', null);
    window.location.href = '';
}

function addNewGroupe() {
    console.log("in add groupe");
    var data = {
        nom: document.getElementById("nomGroupe").value,
        description: 'desc'
    };

    myAjax('https://192.168.75.13/api/v2/groupes', 'POST', JSON.stringify(data))
        .then((d) => {
            myAjax('https://192.168.75.13/api/v2/groupes', 'GET')
                .then(d => {
                    for (i = 0; i < d.length; ++i) {
                        d[i] = d[i].substring(d[i].lastIndexOf('/') + 1);
                    }
                    mustacheTemp("groupesScript", d, "groupesList");
                })
        });
}

function fillGroupes() {
    myAjax('https://192.168.75.13/api/v2/groupes', 'GET')
        .then((d) => {
            for (i = 0; i < d.length; ++i) {
                d[i] = d[i].substring(d[i].lastIndexOf('/') + 1);
            }
            console.log(d);
            mustacheTemp("groupesScript", d, "groupesList");
        });
}


/*----------------------------------------------------------------- */

function fillGroupe(url) {
    if (Cookies.get('Authorization') === 'null') {
        alert("Please Login!");
        window.location.href = '';
        return;
    }

    if (url != null)
        if ($("#selectedGroupe").val() === "" || $("#selectedGroupe").val() !== url)
            $("#selectedGroupe").val(url);

    myAjax(url, 'GET')
        .then((d) => {

            d.billetsD = [];

            for (i = 0; i < d.membres.length; ++i) {
                d.membres[i] = d.membres[i].substring(d.membres[i].lastIndexOf('/') + 1);
            }

            for (i = 0; i < d.billets.length; ++i) {
                d.billetsD[i] = {
                    "billets": d.billets[i],
                    "billetsD": "Billet " + d.billets[i].substring(d.billets[i].lastIndexOf('/') + 1)
                };
            }
            delete d.billets;
            d.auteur = d.auteur.substring(d.auteur.lastIndexOf('/') + 1);
            console.log(d);
            mustacheTemp("groupeScript", d, "grpDesc");
        });
}

function modificationGroupe() {
    myAjax($("#selectedGroupe").val(), 'GET')
        .then((d) => {
            d.description = $("#newDescGroupe").val();
            myAjax($("#selectedGroupe").val(), 'PUT', JSON.stringify(d))
                .then(() => {

                    d.billetsD = [];

                    for (i = 0; i < d.membres.length; ++i) {
                        d.membres[i] = d.membres[i].substring(d.membres[i].lastIndexOf('/') + 1);
                    }

                    for (i = 0; i < d.billets.length; ++i) {
                        d.billetsD[i] = {
                            "billets": d.billets[i],
                            "billetsD": "Billet " + d.billets[i].substring(d.billets[i].lastIndexOf('/') + 1)
                        };
                    }
                    delete d.billets;
                    d.auteur = d.auteur.substring(d.auteur.lastIndexOf('/') + 1);
                    console.log(d);
                    mustacheTemp("groupeScript", d, "grpDesc");
                });
        });

}

function addUserToGroupe() {
    myAjax($("#selectedGroupe").val(), 'GET')
        .then((d) => {
            d.membres.push("https://192.168.75.13/api/v2/users/" + $("#addUser").val());
            console.log(d);
            console.log(d);
            myAjax($("#selectedGroupe").val(), 'PUT', JSON.stringify(d))
                .then(() => {
                    d.billetsD = [];

                    for (i = 0; i < d.membres.length; ++i) {
                        d.membres[i] = d.membres[i].substring(d.membres[i].lastIndexOf('/') + 1);
                    }

                    for (i = 0; i < d.billets.length; ++i) {
                        d.billetsD[i] = {
                            "billets": d.billets[i],
                            "billetsD": "Billet " + d.billets[i].substring(d.billets[i].lastIndexOf('/') + 1)
                        };
                    }
                    delete d.billets;
                    d.auteur = d.auteur.substring(d.auteur.lastIndexOf('/') + 1);
                    console.log(d);
                    mustacheTemp("groupeScript", d, "grpDesc");
                });
        });
}

function removeUserFromGroupe() {
    myAjax($("#selectedGroupe").val(), 'GET')
        .then((d) => {
            var index = d.membres.indexOf("https://192.168.75.13/api/v2/users/" + $("#removeUser").val());
            if (index > -1) {
                d.membres.splice(index, 1);
            }
            myAjax($("#selectedGroupe").val(), 'PUT', JSON.stringify(d))
                .then(() => {
                    d.billetsD = [];

                    for (i = 0; i < d.membres.length; ++i) {
                        d.membres[i] = d.membres[i].substring(d.membres[i].lastIndexOf('/') + 1);
                    }

                    for (i = 0; i < d.billets.length; ++i) {
                        d.billetsD[i] = {
                            "billets": d.billets[i],
                            "billetsD": "Billet " + d.billets[i].substring(d.billets[i].lastIndexOf('/') + 1)
                        };
                    }
                    delete d.billets;
                    d.auteur = d.auteur.substring(d.auteur.lastIndexOf('/') + 1);
                    console.log(d);
                    mustacheTemp("groupeScript", d, "grpDesc");
                });
        });
}

function addNewBillet() {
    console.log("in add billet");
    var data = {
        titre: document.getElementById("titreBillet").value,
        contenu: document.getElementById("contenuBillet").value,
    };

    myAjax(document.getElementById("selectedGroupe").value + "/billets", 'POST', JSON.stringify(data))
        .then(() => {
            myAjax($("#selectedGroupe").val(), 'GET')
                .then(d => {
                    d.billetsD = [];

                    for (i = 0; i < d.membres.length; ++i) {
                        d.membres[i] = d.membres[i].substring(d.membres[i].lastIndexOf('/') + 1);
                    }

                    for (i = 0; i < d.billets.length; ++i) {
                        d.billetsD[i] = {
                            "billets": d.billets[i],
                            "billetsD": "Billet " + d.billets[i].substring(d.billets[i].lastIndexOf('/') + 1)
                        };
                    }
                    delete d.billets;
                    d.auteur = d.auteur.substring(d.auteur.lastIndexOf('/') + 1);
                    mustacheTemp("groupeScript", d, "grpDesc");
                })
        });
}

function fillBillet(url) {
    if (Cookies.get('Authorization') === 'null') {
        alert("Please Login!");
        window.location.href = '';
        return;
    }
    if (url != null)
        if ($("#selectedBillet").val() === "" || $("#selectedBillet").val() !== url)
            $("#selectedBillet").val(url);
    var myHeaders = new Headers();

    myHeaders.set('Accept', 'application/json');
    myHeaders.set('Content-Type', 'application/json');
    myHeaders.set('Authorization', Cookies.get('Authorization'));

    var myInit = {
        method: 'GET',
        headers: myHeaders,
        mode: 'cors',
        cache: 'default'
    };

    fetch(url, myInit)
        .then(response => {
            return response.json();
        })
        .then(myBlob => {
            myBlob.auteur = myBlob.auteur.substring(myBlob.auteur.lastIndexOf('/') + 1);

            console.log(myBlob);
            mustacheTemp("billetScript", myBlob, "bltContenu");
        });

}

function addNewCommentaire() {

    var billet = $("#selectedBillet").val();
    var data = {
        texte: document.getElementById("commentaire").value
    };

    myAjax(billet + '/commentaires', 'POST', JSON.stringify(data))
        .then(() => {
            fillBillet(billet);
        });
    return false;
}

function fillUsers() {
    myAjax('https://192.168.75.13/api/v2/users/', 'GET')
        .then((d) => {
            for (i = 0; i < d.length; ++i) {
                d[i] = d[i].substring(d[i].lastIndexOf('/') + 1);
            }
            mustacheTemp("usersScript", d, "usersList");
        });
}

// function fillUsers() {
// myAjax('https://192.168.75.13/api/v2/users/', 'GET')
// .then((d) => {
// mustacheTemp("usersScript", d, "usersList");
// });
// }

// myAjax('https://192.168.75.13/api/v2/groupes', 'POST')
// .then((d) => {
// console.log(d);
// });