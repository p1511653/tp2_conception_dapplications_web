# Preambule :

## Groupe D

* Simon VERDU : p1509717
* Andrew ALBERT EDWARD EBRAHIM : p1511653

ANDREW a changé de groupe cependant nous ne nous souvenons plus de son groupe initial.


# Section TP2 & TP3 :

## Déploiement : https://192.168.75.43/api/v1
#### Si jamais le lien ci-dessus ne fonctionne pas il est possible que Tomcat ou bien Nginx ai coupé de manière inopportune.  
#### Si tel est le cas n'hésitez pas a nous contacter par téléphone au 06.47.39.74.07 ou bien par email : andrew.albert-edward-ebrahim@etu.univ-lyon1.fr simon.verdu@etu.univ-lyon1.fr    


## Description :

### TP2 :

Configuration de Tomcat : Traité.  
Mise en place du projet : Traité.  
Configuration du projet Maven : Traité.  
Configuration pour l'utilisation d'Expression Language en JSP : Traité.  
Compilation d'un projet Web et Déploiement sur Tomcat : Traité.  
1. Conception basique de l'application  : Traité.  
1.1 Interface de gestion des billets : Traité.  
1.2 Déconnexion : Traité.  
1.3 Ajout d'un menu : Traité.  
1.4 Ajout de groupes : Traité.  
2. Déploiement sur votre VM : Traité.    
2.1 Reconfiguration de nginx : Traité.    
2.2 Installation de Java : Traité.  
2.3 Installation de Tomcat : Traité.  

### TP3 :

1. Refactoring de votre application  : Traité.  
1.1 Pattern contexte : Traité.  
1.2 Pattern MVC : Traité.  
1.2.1 MVC pull-based : Traité.  
1.2.2 MVC Push-based : Traité.  
1.2.3 Contrôle de l'accès aux vues : Traité.  
1.3.1 Authentification : Traité.  
1.3.2 Autorisation : Traité.
2. Gestion du cache  
2.1 Utilisation des en-têtes HTTP : Traité.    
2.2 Utilisation des cookies : Traité.  
2.3 Déploiement : Traité.

# Section TP4 :

## Déploiement : https://192.168.75.43/api/v2/
### Le liens ci dessus redirige vers l'origine de notre API qui n'effectue pas de traitement particulier.  
### Utilisez https://192.168.75.43/api/v2/groupes afin d'avoir un apercu un peu plus pertinant.
#### Si jamais le lien ci-dessus ne fonctionne pas il est possible que Tomcat ou bien Nginx ai coupé de manière inopportune.  
#### Si tel est le cas n'hésitez pas a nous contacter par téléphone au 06.47.39.74.07 ou bien par email : andrew.albert-edward-ebrahim@etu.univ-lyon1.fr , simon.verdu@etu.univ-lyon1.fr    

1. Ressources  : Traité.  
1.1 Modification des URLs : Traité.   
1.1.1 : Traité.  
1.1.2 : Traité.  
1.1.3 : Traité.  
1.2 Représentations des ressources :     
1.2.1 : Contenus des requêtes : Traité.   
1.2.2 : Contenus des réponses : Non Traité.  
1.3 : Sémantique de HTTP : Traité.  
2. Transactions sans état : Non Traité.  
2.1 : Contexte des requêtes : Non Traité.  
2.2 : Authentification Stateless :  Non Traité.  
3. Négociation de contenus : Partiellement Traité.  
3.1 Traité.  
3.2 Traité.  
3.3 Traité.  
3.4 Traité.  
3.5 Traité.  
3.6 Partiellement Traité (uniquement la partie des groupes).  
3.7 Non Traité.  
4.Hypermédia : Non Traité.  
5.Finalisation de l'API : Non Traité.  

# Section TP5 & TP7 :

## Déploiement: https://192.168.75.43
#### Si jamais le lien ci-dessus ne fonctionne pas il est possible que Tomcat ou bien Nginx ai coupé de manière inopportune.  
#### Si tel est le cas n'hésitez pas a nous contacter par téléphone au 06.47.39.74.07 ou bien par email : andrew.albert-edward-ebrahim@etu.univ-lyon1.fr simon.verdu@etu.univ-lyon1.fr    


### TP5 :   

1. Première application AJAX : Traité.  
1.1 Transformation XSLT : Traité.  
 Nous n'avons cependant pas mis cette partie sur notre depot GIT.
2. Reprise de votre application de blog : Traité.  
2.1 Single-Page Application : Traité.  
2.2 Templating : Traité.
2.2.1 : Traité.  
2.2.2 : Traité.  
2.2.3 : Traité.  
2.2.4 : Traité.  
2.2.5 : Traité.  
2.3 : AJAX : Traité.
2.4 : Fetch API : Traité.  
2.5 : Finalisation de votre application : Traité.

### TP7 :

Lors de l'affichage de notre page d'accueil le navigateur charge les fichiers suivants :
- / (<- index.html)  
- mustache.min.js  
- boostrap.min.css 
- jquery.min.js
- js.cookies.min.js  
- script.js  (<- contient les scripts d'interaction avec l'API ainsi que ceux nécessaires a l'affichage dynamique de notre application)

#### Indiquer les éléments du DOM faisant partie de l’app shell :  
D'apres nos recherches nous en sommes venu a la conclusion que l'app shell correspond a l'ensemble des elements nécessaire a ce que la page soit fonctionnelle.  
Afin que notre page d'accueil soit fonctionnel il faut donc que l'ensemble des fichiers soit chargés a l'exception de mustache.min.js. mustache.min.js. Mustache une  librairie qui permet d'utiliser le système de templating.  
Etant donné que nous n'utilisons pas ce système dans notre page d'accueil le chargement de cette librairie n'est donc pas nécessaire pour que la page d'accueil soit fonctionnelle.  


#### Indiquer les éléments du DOM faisant partie du CRP :  
CRP : Suite d'étapes effectués par le navigateur afin de convertir le code HTML, CSS, et JavaScript en pixel sur l'écran.( source : developer.mozilla.org)  
Afin que notre page d'accueil soit entièrement affichée a l'écran  nous avons donc besoin de chager l'intégralité des fichiers a l'exception du fichier mustache.min.js (pour les memes raisons que celles expliquées précédemment) et de js.cookies.min.js.
En effet js-cookies est la librairie qui nous permet de gérer les cookies lors de nos interractions avec l'API, elle n'est donc pas utilisée pour l'affichage de notre page d'accueil.

#### déploiement sur Tomcat : 

Le temps de chargement de la page HTML initiale : 327 (ms)

Le temps d'affichage de l'app shell : 332 (ms)

Le temps d'affichage du chemin critique de rendu (CRP) : 337 (ms)

#### déploiement sur nginx : 

TempsTomcat --> 100 %.  
TempsNginx --> X %.  
X = (TempsNginx*100)/TempsTomcat.  
Le champs X correspond au pourcentage d'amélioration.  

Le temps de chargement de la page HTML initiale : 258 (ms) -> Pourcentage d'amélioration :  22%.   

Le temps d'affichage de l'app shell : 262 (ms) -> Pourcentage d'amélioration : 22%.

Le temps d'affichage du chemin critique de rendu (CRP) : 267 (ms) -> Pourcentage d'amélioration : 21%.

#### Optimisation de votre application : 

- Utilisation de CDN :  
    - Nous avons choisi dès le depart d'utiliser des CDN lorsque nous avions besoins de charger des librairies externes (boostrap, jquery, mustache, js-cookie ...).
      De ce fait nous n'avons pas eu de modifications a faire lors de cette partie.
- Utilisation d'attributs async et/ou defer pour décaler le chargement de scripts non nécessaires au CRP :
    - Lors de l'affichage de la page d'accueil nous ne lancions pas de requetes vers l'API car premièrement il faut etre authentifié pour pouvoir effectuer la plupart des requetes.
     Nous aurions pu cependant requeter l'API afin d'obtenir la liste des groupes car cette requete ne nécessite pas d'authentification.
     Mais étant donné que notre page d'accueil est un formulaire de connexion et que nous affichons les groupes dans une autre page prévue a cet effet.
     Nous avons choisi de requêter l'API au moment ou l'utilisateur souhaite consulter la liste des groupes.
     Nous n'avons donc pas eu de modifications a faire dans cette partie puisque les scripts que nous utilisions n'avaient pas d'impact sur le chargement du CRP. 
- Minification réduction du nombre de ressources critiques : 
  Notre application est composé uniquement d'un fichier HTML et d'un fichier JS nous avions mis l'ensemble du contenu de notre fichier JS dans notre fichier HTML.
  Nous n'avons cependant pas remarqué de gain de performance. Au final nous avons choisis de garder un fichier HTML et un fichier JS par soucis de lisibilité pour les développeurs.

Initialement le rapport d'audit nous affichais les résultats suivants :  
Performance : 99  
Accessibility : 63  
Best Practices : 93  
SEO : 75  

Afin d'améliorer la note du champs accessibilité Accessibility nous avons : 

- Changer la couleur des champs du menu initialement ils etaient gris, nous les avons mis en noir. 
Le site est selon nous moins esthétique, cependant ceci nous a permis de régler le problème : "Background and foreground colors do not have a sufficient contrast ratio."
- Ajouter un <label> autour l'input du pseudo dans le fichier index.html pour régler le problème : "Form elements do not have associated labels"


```html
<label> votre pseudo :
    <input type="text" id="pseudo">
    <input type="submit" value="Connexion">
</label>
```

Afin d'améliorer la note du champs accessibilité SEO nous avons : 

-Ajouter <meta name ="viewport" content="width=device-width, initial-scale=1"> afin de régler le problème : "Does not have a <meta name="viewport"> tag with width or initial-scale"

-Ajouter <meta name="Description" content="Description"> pour régler le problème : "Document does not have a meta description"

Au final le rapport d'audit nous affichais les résultats suivants :  

Performance : 99  
Accessibility : 100  
Best Practices : 93  
SEO : 100  

Nous n'avons pas réussit a obtenir de meilleurs resultats cependant ces derniers nous on sembé plutot bons.